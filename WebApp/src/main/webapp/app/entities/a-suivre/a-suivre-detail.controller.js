(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ASuivreDetailController', ASuivreDetailController);

    ASuivreDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ASuivre'];

    function ASuivreDetailController($scope, $rootScope, $stateParams, previousState, entity, ASuivre) {
        var vm = this;

        vm.aSuivre = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('webApp:aSuivreUpdate', function(event, result) {
            vm.aSuivre = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
