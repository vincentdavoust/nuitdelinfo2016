(function() {
    'use strict';
    angular
        .module('webApp')
        .factory('ASuivre', ASuivre);

    ASuivre.$inject = ['$resource'];

    function ASuivre ($resource) {
        var resourceUrl =  'api/a-suivres/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
