(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ASuivreDeleteController',ASuivreDeleteController);

    ASuivreDeleteController.$inject = ['$uibModalInstance', 'entity', 'ASuivre'];

    function ASuivreDeleteController($uibModalInstance, entity, ASuivre) {
        var vm = this;

        vm.aSuivre = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ASuivre.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
