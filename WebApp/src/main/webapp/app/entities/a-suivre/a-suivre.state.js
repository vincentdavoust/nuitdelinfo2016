(function() {
    'use strict';

    angular
        .module('webApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('a-suivre', {
            parent: 'entity',
            url: '/a-suivre',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'webApp.aSuivre.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/a-suivre/a-suivres.html',
                    controller: 'ASuivreController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('aSuivre');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('a-suivre-detail', {
            parent: 'entity',
            url: '/a-suivre/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'webApp.aSuivre.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/a-suivre/a-suivre-detail.html',
                    controller: 'ASuivreDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('aSuivre');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ASuivre', function($stateParams, ASuivre) {
                    return ASuivre.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'a-suivre',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('a-suivre-detail.edit', {
            parent: 'a-suivre-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a-suivre/a-suivre-dialog.html',
                    controller: 'ASuivreDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ASuivre', function(ASuivre) {
                            return ASuivre.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('a-suivre.new', {
            parent: 'a-suivre',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a-suivre/a-suivre-dialog.html',
                    controller: 'ASuivreDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                idAsuivre: null,
                                tag: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('a-suivre', null, { reload: 'a-suivre' });
                }, function() {
                    $state.go('a-suivre');
                });
            }]
        })
        .state('a-suivre.edit', {
            parent: 'a-suivre',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a-suivre/a-suivre-dialog.html',
                    controller: 'ASuivreDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ASuivre', function(ASuivre) {
                            return ASuivre.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('a-suivre', null, { reload: 'a-suivre' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('a-suivre.delete', {
            parent: 'a-suivre',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a-suivre/a-suivre-delete-dialog.html',
                    controller: 'ASuivreDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ASuivre', function(ASuivre) {
                            return ASuivre.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('a-suivre', null, { reload: 'a-suivre' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
