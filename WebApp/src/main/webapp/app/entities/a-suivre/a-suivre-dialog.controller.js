(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ASuivreDialogController', ASuivreDialogController);

    ASuivreDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ASuivre'];

    function ASuivreDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ASuivre) {
        var vm = this;

        vm.aSuivre = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.aSuivre.id !== null) {
                ASuivre.update(vm.aSuivre, onSaveSuccess, onSaveError);
            } else {
                ASuivre.save(vm.aSuivre, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('webApp:aSuivreUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
