(function() {
    'use strict';

    angular
        .module('webApp')
        .controller('ASuivreController', ASuivreController);

    ASuivreController.$inject = ['$scope', '$state', 'ASuivre'];

    function ASuivreController ($scope, $state, ASuivre) {
        var vm = this;

        vm.aSuivres = [];

        loadAll();

        function loadAll() {
            ASuivre.query(function(result) {
                vm.aSuivres = result;
                vm.searchQuery = null;
            });
        }
    }
})();
