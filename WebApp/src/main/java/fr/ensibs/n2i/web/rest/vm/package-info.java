/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.ensibs.n2i.web.rest.vm;
