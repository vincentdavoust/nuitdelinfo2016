package fr.ensibs.n2i.web.rest;

import com.codahale.metrics.annotation.Timed;
import fr.ensibs.n2i.domain.ASuivre;

import fr.ensibs.n2i.repository.ASuivreRepository;
import fr.ensibs.n2i.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ASuivre.
 */
@RestController
@RequestMapping("/api")
public class ASuivreResource {

    private final Logger log = LoggerFactory.getLogger(ASuivreResource.class);
        
    @Inject
    private ASuivreRepository aSuivreRepository;

    /**
     * POST  /a-suivres : Create a new aSuivre.
     *
     * @param aSuivre the aSuivre to create
     * @return the ResponseEntity with status 201 (Created) and with body the new aSuivre, or with status 400 (Bad Request) if the aSuivre has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/a-suivres")
    @Timed
    public ResponseEntity<ASuivre> createASuivre(@Valid @RequestBody ASuivre aSuivre) throws URISyntaxException {
        log.debug("REST request to save ASuivre : {}", aSuivre);
        if (aSuivre.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("aSuivre", "idexists", "A new aSuivre cannot already have an ID")).body(null);
        }
        ASuivre result = aSuivreRepository.save(aSuivre);
        return ResponseEntity.created(new URI("/api/a-suivres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("aSuivre", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /a-suivres : Updates an existing aSuivre.
     *
     * @param aSuivre the aSuivre to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated aSuivre,
     * or with status 400 (Bad Request) if the aSuivre is not valid,
     * or with status 500 (Internal Server Error) if the aSuivre couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/a-suivres")
    @Timed
    public ResponseEntity<ASuivre> updateASuivre(@Valid @RequestBody ASuivre aSuivre) throws URISyntaxException {
        log.debug("REST request to update ASuivre : {}", aSuivre);
        if (aSuivre.getId() == null) {
            return createASuivre(aSuivre);
        }
        ASuivre result = aSuivreRepository.save(aSuivre);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("aSuivre", aSuivre.getId().toString()))
            .body(result);
    }

    /**
     * GET  /a-suivres : get all the aSuivres.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of aSuivres in body
     */
    @GetMapping("/a-suivres")
    @Timed
    public List<ASuivre> getAllASuivres() {
        log.debug("REST request to get all ASuivres");
        List<ASuivre> aSuivres = aSuivreRepository.findAll();
        return aSuivres;
    }

    /**
     * GET  /a-suivres/:id : get the "id" aSuivre.
     *
     * @param id the id of the aSuivre to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the aSuivre, or with status 404 (Not Found)
     */
    @GetMapping("/a-suivres/{id}")
    @Timed
    public ResponseEntity<ASuivre> getASuivre(@PathVariable Long id) {
        log.debug("REST request to get ASuivre : {}", id);
        ASuivre aSuivre = aSuivreRepository.findOne(id);
        return Optional.ofNullable(aSuivre)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /a-suivres/:id : delete the "id" aSuivre.
     *
     * @param id the id of the aSuivre to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/a-suivres/{id}")
    @Timed
    public ResponseEntity<Void> deleteASuivre(@PathVariable Long id) {
        log.debug("REST request to delete ASuivre : {}", id);
        aSuivreRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("aSuivre", id.toString())).build();
    }

}
