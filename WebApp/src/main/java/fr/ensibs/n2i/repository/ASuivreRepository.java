package fr.ensibs.n2i.repository;

import fr.ensibs.n2i.domain.ASuivre;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ASuivre entity.
 */
@SuppressWarnings("unused")
public interface ASuivreRepository extends JpaRepository<ASuivre,Long> {

}
