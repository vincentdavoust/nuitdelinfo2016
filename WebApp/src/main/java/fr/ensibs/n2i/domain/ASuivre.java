package fr.ensibs.n2i.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ASuivre.
 */
@Entity
@Table(name = "asuivre")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ASuivre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "id_asuivre", nullable = false)
    private Integer idAsuivre;

    @Column(name = "tag")
    private String tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdAsuivre() {
        return idAsuivre;
    }

    public ASuivre idAsuivre(Integer idAsuivre) {
        this.idAsuivre = idAsuivre;
        return this;
    }

    public void setIdAsuivre(Integer idAsuivre) {
        this.idAsuivre = idAsuivre;
    }

    public String getTag() {
        return tag;
    }

    public ASuivre tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ASuivre aSuivre = (ASuivre) o;
        if (aSuivre.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, aSuivre.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ASuivre{" +
            "id=" + id +
            ", idAsuivre='" + idAsuivre + "'" +
            ", tag='" + tag + "'" +
            '}';
    }
}
