package fr.ensibs.n2i.web.rest;

import fr.ensibs.n2i.WebApp;

import fr.ensibs.n2i.domain.ASuivre;
import fr.ensibs.n2i.repository.ASuivreRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ASuivreResource REST controller.
 *
 * @see ASuivreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApp.class)
public class ASuivreResourceIntTest {

    private static final Integer DEFAULT_ID_ASUIVRE = 1;
    private static final Integer UPDATED_ID_ASUIVRE = 2;

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    @Inject
    private ASuivreRepository aSuivreRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restASuivreMockMvc;

    private ASuivre aSuivre;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ASuivreResource aSuivreResource = new ASuivreResource();
        ReflectionTestUtils.setField(aSuivreResource, "aSuivreRepository", aSuivreRepository);
        this.restASuivreMockMvc = MockMvcBuilders.standaloneSetup(aSuivreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ASuivre createEntity(EntityManager em) {
        ASuivre aSuivre = new ASuivre()
                .idAsuivre(DEFAULT_ID_ASUIVRE)
                .tag(DEFAULT_TAG);
        return aSuivre;
    }

    @Before
    public void initTest() {
        aSuivre = createEntity(em);
    }

    @Test
    @Transactional
    public void createASuivre() throws Exception {
        int databaseSizeBeforeCreate = aSuivreRepository.findAll().size();

        // Create the ASuivre

        restASuivreMockMvc.perform(post("/api/a-suivres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aSuivre)))
            .andExpect(status().isCreated());

        // Validate the ASuivre in the database
        List<ASuivre> aSuivres = aSuivreRepository.findAll();
        assertThat(aSuivres).hasSize(databaseSizeBeforeCreate + 1);
        ASuivre testASuivre = aSuivres.get(aSuivres.size() - 1);
        assertThat(testASuivre.getIdAsuivre()).isEqualTo(DEFAULT_ID_ASUIVRE);
        assertThat(testASuivre.getTag()).isEqualTo(DEFAULT_TAG);
    }

    @Test
    @Transactional
    public void checkIdAsuivreIsRequired() throws Exception {
        int databaseSizeBeforeTest = aSuivreRepository.findAll().size();
        // set the field null
        aSuivre.setIdAsuivre(null);

        // Create the ASuivre, which fails.

        restASuivreMockMvc.perform(post("/api/a-suivres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(aSuivre)))
            .andExpect(status().isBadRequest());

        List<ASuivre> aSuivres = aSuivreRepository.findAll();
        assertThat(aSuivres).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllASuivres() throws Exception {
        // Initialize the database
        aSuivreRepository.saveAndFlush(aSuivre);

        // Get all the aSuivres
        restASuivreMockMvc.perform(get("/api/a-suivres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aSuivre.getId().intValue())))
            .andExpect(jsonPath("$.[*].idAsuivre").value(hasItem(DEFAULT_ID_ASUIVRE)))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG.toString())));
    }

    @Test
    @Transactional
    public void getASuivre() throws Exception {
        // Initialize the database
        aSuivreRepository.saveAndFlush(aSuivre);

        // Get the aSuivre
        restASuivreMockMvc.perform(get("/api/a-suivres/{id}", aSuivre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(aSuivre.getId().intValue()))
            .andExpect(jsonPath("$.idAsuivre").value(DEFAULT_ID_ASUIVRE))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingASuivre() throws Exception {
        // Get the aSuivre
        restASuivreMockMvc.perform(get("/api/a-suivres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateASuivre() throws Exception {
        // Initialize the database
        aSuivreRepository.saveAndFlush(aSuivre);
        int databaseSizeBeforeUpdate = aSuivreRepository.findAll().size();

        // Update the aSuivre
        ASuivre updatedASuivre = aSuivreRepository.findOne(aSuivre.getId());
        updatedASuivre
                .idAsuivre(UPDATED_ID_ASUIVRE)
                .tag(UPDATED_TAG);

        restASuivreMockMvc.perform(put("/api/a-suivres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedASuivre)))
            .andExpect(status().isOk());

        // Validate the ASuivre in the database
        List<ASuivre> aSuivres = aSuivreRepository.findAll();
        assertThat(aSuivres).hasSize(databaseSizeBeforeUpdate);
        ASuivre testASuivre = aSuivres.get(aSuivres.size() - 1);
        assertThat(testASuivre.getIdAsuivre()).isEqualTo(UPDATED_ID_ASUIVRE);
        assertThat(testASuivre.getTag()).isEqualTo(UPDATED_TAG);
    }

    @Test
    @Transactional
    public void deleteASuivre() throws Exception {
        // Initialize the database
        aSuivreRepository.saveAndFlush(aSuivre);
        int databaseSizeBeforeDelete = aSuivreRepository.findAll().size();

        // Get the aSuivre
        restASuivreMockMvc.perform(delete("/api/a-suivres/{id}", aSuivre.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ASuivre> aSuivres = aSuivreRepository.findAll();
        assertThat(aSuivres).hasSize(databaseSizeBeforeDelete - 1);
    }
}
