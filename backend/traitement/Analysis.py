##
## Analysis.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Thu Dec  3 21:25:10 2015 Vincent Davoust
## Last update Thu Dec  1 22:43:20 2016 Vincent Davoust
##

import sys
import mysql.connector
sys.path.append("../commun")
from message import Message


class Analysis:


    # ok
    def __init__(self) :
        self.db = mysql.connector.connect(host="localhost",user="root",password="benjamin", database="WebApp")
        self.cursor = self.db.cursor()

    # should be ok
    def count(self, msg):
        # querry to count number of occurences where match is close
        query = "SELECT COUNT(*) AS nb\
                        FROM messages \
                        WHERE MATCH (messages.contenu) \
                        AGAINST ('"+msg.c+"') > " + '5'

        # execute SQL select statement
        self.cursor.execute(query)

        # commit your changes
        self.db.commit()

        row = self.cursor.fetchone()
        msg.setCount(row[0])

    # not ok
    def grade(self, msg):
        query = "SELECT (MATCH (words) \
                        AGAINST ('"+msg.c+"' \
                        IN NATURAL LANGUAGE MODE)) * (SELECT tauxinfosok FROM compte WHERE nom='"+msg.a+"') * \
                        (SELECT MATCH() AGAINST ('"+msg.c+"' IN NATURAL LANGUAGE MODE)) AS grade \
                        FROM keywords"

        # execute SQL select statement
        self.cursor.execute(query)

        # commit your changes
        self.db.commit()

        row = self.cursor.fetchone()
        msg.setGrade(row[0])

    # not ok, fiab formula to be corrected
    def getfiab(self, msg) :
        query = "SELECT tauxinfosok * volumeflux \
                 FROM compte \
                 WHERE nom="+msg.a

        # execute SQL select statement
        self.cursor.execute(query)

        # commit your changes
        self.db.commit()

        row = self.cursor.fetchone()
        msg.setFiab(row[0])

    # should be ok
    def getevent(self, msg):
        query = "SELECT id \
                 FROM messages, message_event \
                 WHERE message.id = message_event.id \
                 ORDER BY MATCH (contenu) \
                 AGAINST ('"+msg.c+"' IN NATURAL LANGUAGE MODE) \
                 LIMIT 1,1"

        # execute SQL select statement
        self.cursor.execute(query)

        # commit your changes
        self.db.commit()

        row = self.cursor.fetchone()
        msg.setEvent(row[0])


