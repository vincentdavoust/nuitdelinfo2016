#!/bin/env python
##
## main.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Thu Dec  3 21:25:10 2015 Vincent Davoust
## Last update Thu Dec  1 22:42:17 2016 Vincent Davoust
##

import socket
import sys
sys.path.append("../commun")
from message import Message
from Analysis import Analysis
from rules import Rules
from save import save
## infinite loop
##      listen to data input
##      for each packet : 

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('', 50007))

mind = Analysis()

while (True):
    # get message
    tmp = sock.recv(10000)
    tmp = tmp.split(';')
    msg = Message(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4])
    #    msg = Message(0, 'param', 'date', 'auteur', 'contenu') # read from socket here
    print msg
    # update rules
    rules = Rules()

    mind.count(msg)
    mind.grade(msg)
    mind.contradictions(msg, rules)
    save(msg)
