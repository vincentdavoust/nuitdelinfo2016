##
## rules.py for main in /home/vincent/nuit_info/APP - Paul(C) et Vincent/traitement
## 
## Made by Vincent Davoust
## <vincent.davoust@gmail.com>
## 
## Started on  Thu Dec  3 21:25:10 2015 Vincent Davoust
## Last update Thu Dec  1 22:40:25 2016 Vincent Davoust
##

import sys
import mysql

sys.path.append("../commun")
from message import Message


class Rules:
    def __init__(self):
        self.keywords = 0
        self.events = 0
        self.connect()
        self.getData()
        self.db = 0
        self.cursor = 0

    def connect(self):
        # connect to bdd
        self.db = mysql.connector.connect(host="localhost",user="root",password="benjamin", database="WebApp")
        self.cursor = self.db.cursor()

    def getdata(self):
        self.connect()
        # get keywords from bdd
        query = "SELECT * FROM keywords"
        # execute SQL select statement
        self.cursor.execute(query)
        # commit your changes
        self.db.commit()
        # put in public var
        self.keywords = self.cursor.fetch()

        # get events from bdd
        query = "SELECT * FROM events"
        # execute SQL select statement
        self.cursor.execute(query)
        # commit your changes
        self.db.commit()
        # put in public var
        self.events = self.cursor.fetch()
