-- BDD nuit de l'info 2016

-- Partie Paul
DROP TABLE IF EXISTS ASuivre;

-- Partie VincentD
DROP TABLE IF EXISTS News;
DROP TABLE IF EXISTS Message_event;
DROP TABLE IF EXISTS Keyword_message;
DROP TABLE IF EXISTS Source;
DROP TABLE IF EXISTS Compte;
DROP TABLE IF EXISTS Message;
DROP TABLE IF EXISTS Hashtag_event;
DROP TABLE IF EXISTS Event;
DROP TABLE IF EXISTS Hashtag;
DROP TABLE IF EXISTS Keyword;


-- Partie VincentD
CREATE TABLE Keyword (
	idkeyword INT AUTO_INCREMENT,
	word TEXT NOT NULL,
	priorite INT NOT NULL,
	PRIMARY KEY (idkeyword)
);

CREATE TABLE Hashtag (
	idht INT AUTO_INCREMENT,
	tag TEXT NOT NULL,
	PRIMARY KEY (idht)
);

CREATE TABLE Event (
	idevent INT AUTO_INCREMENT,
	intitule TEXT NOT NULL,
	datestart TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	datefin TIMESTAMP DEFAULT '2038-01-19 03:14:07',
	PRIMARY KEY (idevent)
);

CREATE TABLE Hashtag_event (
	hashtag INT,
	event INT,
	PRIMARY KEY (hashtag, event),
	FOREIGN KEY (hashtag) REFERENCES Hashtag(idht),
	FOREIGN KEY (event) REFERENCES Event(idevent)
);

CREATE TABLE Message (
	idmessage INT AUTO_INCREMENT,
	contenu TEXT NOT NULL,
	pertinence FLOAT NOT NULL,
	admin INT,
	PRIMARY KEY (idmessage)
);

CREATE TABLE Compte (
	idcompte INT AUTO_INCREMENT,
	nom TEXT NOT NULL,
	volumeflux INT NOT NULL,
	tauxinfosok FLOAT NOT NULL,
	type TEXT NOT NULL,
	PRIMARY KEY (idcompte)
);

CREATE TABLE Source (
	idsource INT AUTO_INCREMENT,
	contenu TEXT NOT NULL,
	source CHAR NOT NULL,
	compte INT NOT NULL,
	datepost TIMESTAMP NOT NULL,
	message INT NOT NULL,
	PRIMARY KEY (idsource),
	FOREIGN KEY (message) REFERENCES Message(idmessage),
	FOREIGN KEY (compte) REFERENCES Compte(idcompte)
);

CREATE TABLE Keyword_message (
	keyword INT,
	message INT,
	PRIMARY KEY (keyword, message),
	FOREIGN KEY (keyword) REFERENCES Keyword(idkeyword),
	FOREIGN KEY (message) REFERENCES Message(idmessage)
);

CREATE TABLE Message_event (
	message INT,
	event INT,
	PRIMARY KEY (message, event),
	FOREIGN KEY (message) REFERENCES Message(idmessage),
	FOREIGN KEY (event) REFERENCES Event(idevent)
);

CREATE TABLE News (
	idNews INT AUTO_INCREMENT,
	titre TEXT,
	auteur TEXT,
	contenu TEXT,
	date_publication TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	langue TEXT,
	PRIMARY KEY (idNews)
);

-- Partie Paul
CREATE TABLE ASuivre (
	idASuivre INT AUTO_INCREMENT,
	tag TEXT,
	PRIMARY KEY(idASuivre)
);
