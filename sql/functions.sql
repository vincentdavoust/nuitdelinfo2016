
DROP TRIGGER IF Exists checkMessage;

DROP FUNCTION IF EXISTS delASuivre;
DROP FUNCTION IF EXISTS addASuivre;

DELIMITER $$

CREATE FUNCTION addASuivre (cat INT, target TEXT) RETURNS INT
BEGIN
	IF cat = 1 THEN
		INSERT INTO ASuivre(tag)
		VALUES( CONCAT('fc:',target) );
		RETURN 1;
	ELSEIF cat = 2 THEN
		INSERT INTO ASuivre(tag)
		VALUES( CONCAT('fg:',target) );
		RETURN 1;
	ELSEIF cat = 3 THEN
		INSERT INTO ASuivre(tag)
		VALUES( CONCAT('th:#',target) );
		RETURN 1;
	ELSEIF cat = 4 THEN
		INSERT INTO ASuivre(tag)
		VALUES( CONCAT('tc:@',target) );
		RETURN 1;
	ELSE
		RETURN 0;
	END IF;
END $$

CREATE FUNCTION delASuivre (cat INT, target TEXT) RETURNS INT
BEGIN
	IF cat = 1 THEN
		DELETE FROM ASuivre
		WHERE tag LIKE CONCAT('fc:',target);
		RETURN ROW_COUNT();
	ELSEIF cat = 2 THEN
		DELETE FROM ASuivre
		WHERE tag LIKE CONCAT('fg:',target);
		RETURN ROW_COUNT();
	ELSEIF cat = 3 THEN
		DELETE FROM ASuivre
		WHERE tag LIKE CONCAT('th:#',target);
		RETURN ROW_COUNT();
	ELSEIF cat = 4 THEN
		DELETE FROM ASuivre
		WHERE tag LIKE CONCAT('tc:@',target);
		RETURN ROW_COUNT();
	ELSE
		RETURN 0;
	END IF;
END $$

--

CREATE TRIGGER checkMessage BEFORE INSERT
ON Message
FOR EACH ROW
BEGIN
	IF new.pertinence <= 0 OR new.pertinence >=1 THEN
		CALL raise_application_error(-20101, 'ERROR: pertinence must be between 0 and 1');
	END IF;
END $$

DELIMITER ;

